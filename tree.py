# -*- coding: utf-8 -*-

from sys import maxint
import itertools

class Tree:    
    def __init__(self):
        self.nodes = []
        self.lb = -maxint
        self.ub = maxint
        self.best_sol = None
        self.solInt = []

    def __str__(self):
        s = ""
        s = "Tree with %d open nodes: " % len(self.nodes)
        return s

    def prune(self, node):
        if node.ub < self.ub:
            self.best_sol = node.sol
            self.ub = node.ub
            self.solInt = node.solInt
            for n in self.nodes:
                if n.lb > self.ub:
                    self.nodes.remove(n)
                    

class Node:
    ix = itertools.count(0)

    def __init__(self, pix, depth, fix1, fix0, parcels, set_parcels):
        self.ix = next(Node.ix)
        self.pix = pix
        self.depth = depth
        self.fix1 = fix1
        self.fix0 = fix0
        self.parcels = parcels
        self.set_parcels = set_parcels
        self.children = []
        self.lb = -maxint
        self.ub = maxint
        self.sol = {}
        self.solInt = []
        self.duals = {}
        
    def __str__(self):
        s = ""
        s = 'Node. ix:%d, pix:%d, depth:%d, lb:%4.f  ' %(self.ix, self.pix, self.depth, self.lb)
        s += "fix1: " + ','.join(self.fix1) 
        s += "fix0: " + ','.join(self.fix0) 
        return s

    def branch(self):
        branching_parcel = None
        min_fract = 1
#        print(type(self.sol))
        for p,v in self.sol.items():
            if v > 0.0001:
                fract = abs(v-0.5)
                if fract < min_fract:
                    min_fract = fract
                    branching_parcel = p

        # branching now
        fix1_new = self.fix1[:]
        fix1_new.append(branching_parcel)
        node1 = Node(self.ix, self.depth+1,fix1_new, self.fix0, self.parcels, self.set_parcels)
        fix0_new = self.fix0[:]
#        print("node1 fix1")
#        print(node1.fix1)
        fix0_new.append(branching_parcel)
        node0 = Node(self.ix, self.depth+1,self.fix1, fix0_new, self.parcels, self.set_parcels)
#        print("node0 fix1")
#        print(node0.fix1)

        return [node1, node0]

    
