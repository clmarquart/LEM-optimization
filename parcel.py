import networkx as nx
import itertools

class Parcel:
    pix = itertools.count(0)
    
    def __init__(self, G, V, zone):
        self.ix = next(Parcel.pix)
        # This is to just remind me of the members       
        self.V = V
        self.land_use = None
        self.area = None
        self.cost = None
        self.zone = zone
        
        AL = {}
           
        # Determine area of parcel           
        area = 0.0
        for k, d in G.nodes(data=True):
            if k in V:
                av = d['area']
                luv = d['land_use']
                if luv in AL:
                    AL[luv] = AL[luv] + av
                else:
                    AL[luv] = av
                area += av
        
        self.area = area

        # Which land use is the max
        argmax = max(AL, key=lambda key: AL[key])
        self.land_use = argmax

        # Make the cost for the parcel
        cost = 0.0
        for k, d in G.nodes(data=True):
            if k in V:
                av = d['area']
                luv = d['land_use']
                if luv != self.land_use:
                    cost += av
        self.cost = cost

    def __str__(self):
        s = ""
        s = "Parcel %d with vertices: " % self.ix
        for v in self.V:
            s += " " + str(v)
        s += ".  Area: %.2f, Land Use: %s, Cost: %.2f" % (self.area, self.land_use, self.cost)
        return s
