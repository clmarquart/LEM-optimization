# -*- coding: utf-8 -*-
"""
Created on Tue Apr  17 08:29:19 2018

@author: jnares
"""
import networkx as nx
from sys import maxint
from math import ceil
from parcel import Parcel
import random
from feasibility import feas_general, feas_parcel, feas_solution, connected_parcel
import operator
import sys
import itertools
from shapely.geometry import Polygon
from shapely.ops import cascaded_union
import geojson

def divideParcel(G, parcel, max_area, parcels, options):
    if not connected_parcel(G, parcel, options):
        print(parcel)
        print(G.nodes())
        print(G.edges())
        sys.exit("Error: something very bad happened, connected component is not connected!")

    #print("parcel.area = " + str(parcel.area))
    if parcel.area < max_area or len(parcel.V) == 1:
        parcels[parcel.area] = parcel
        #print("in divideParcel, parcel.ara < max.area), checking feasibility")
        #print(feas_parcel(G, parcel, options))
        return True
    # here probably needs to come something for the case when
    # there is one parcel with only one LUC and size bigger than max_area
    # I think it is the only case not covered yet

    G2 = G.copy()
    for v in G.nodes():
        if v not in parcel.V:
            G2.remove_node(v)

    land_use = nx.get_node_attributes(G2,'land_use')
    zonev = nx.get_node_attributes(G,'zone')
    zone = parcel.zone
    for edge in G2.edges():
        if land_use[edge[0]] == land_use[edge[1]]:
            G2[edge[0]][edge[1]]['weight'] = 0.000001
        else:
            G2[edge[0]][edge[1]]['weight'] = 10

    # minimum spanning tree
    #print("graph G2")
    #print(G2.nodes())
    T = nx.minimum_spanning_tree(G2)
    #print("minimum spanning tree T")
    #print(T.nodes())
    #print(len(T.nodes()))
    allSameLUC = True
    costs = {}
    for edge in T.edges(data=True):
        if edge[2]['weight'] > 1:
            allSameLuc = False
            # remove edge and obtain the two connected components of T
            T.remove_edge(*edge[:2])
            components = nx.connected_components(T)
            set1 = next(components)
            set2 = next(components)
            #print("components")
            #print(set1)
            #print(len(set1))
            #print(set2)
            #print(len(set2))
            p1 = Parcel(G, frozenset(set1),zone)
            p2 = Parcel(G, frozenset(set2),zone)
            if feas_parcel(G, p1, options) and p2.area > options['Min_Area']:
                costs[p1] = (p1.cost + 1) / p1.area
            if feas_parcel(G, p2, options) and p1.area > options['Min_Area']:
                costs[p2] = (p2.cost + 1) / p2.area
            # put edge back
            T.add_edge(*edge[:2])

    if allSameLUC:
        for edge in T.edges(data=True):
            # remove edge and obtain the two connected components of T
            T.remove_edge(*edge[:2])
            components = nx.connected_components(T)
            set1 = next(components)
            set2 = next(components)
            #print("components")
            #print(set1)
            #print(len(set1))
            #print(set2)
            #print(len(set2))
            p1 = Parcel(G, frozenset(set1),zone)
            p2 = Parcel(G, frozenset(set2),zone)
            if feas_parcel(G, p1, options) and p2.area > options['Min_Area']:
                costs[p1] = (p1.cost + 1) / p1.area
            if feas_parcel(G, p2, options) and p1.area > options['Min_Area']:
                costs[p2] = (p2.cost + 1) / p2.area
            # put edge back
            T.add_edge(*edge[:2])
        
        
    # select the parcel with lower score
    sorted_costs = sorted(costs.items(), key=operator.itemgetter(1))
    found = False
    ind = -1
    #print("len(sorted_costs): " + str(len(sorted_costs)))
    #print(sorted_costs)
    while not found and ind < len(sorted_costs)-1:
        ind += 1
        #print("ind: " + str(ind))
        if sorted_costs[ind][0].area < max_area:
            found = True

    # retrieve new parcel if found
    if found:
        p0 = sorted_costs[ind][0]
        parcels[p0.area] = p0
        p1 = Parcel(G,parcel.V.difference(p0.V), zone)
        #print("parcel with " + str(len(p0.V)) + " vertices added, with:")
        #print(p0)
        #print("still, for this zone, " + str(len(p1.V)) + " vertices to go")
        divideParcel(G, p1, max_area, parcels, options)
    else:
        p0 = sorted_costs[0][0]
        p1 = Parcel(G,parcel.V.difference(p0.V), zone)
        divideParcel(G, p0, max_area, parcels, options)
        divideParcel(G, p1, max_area, parcels, options)
        

def disaggregate(G,sol_parcels,options):
    # for each calculate the best cut
    dis_costs = {}
    correspondance = {}
    dis_parcels = (x for x in sol_parcels if len(x.V)>1)
    for parcel in dis_parcels:
        G2 = G.copy()
        for v in G.nodes():
            if v not in parcel.V:
                G2.remove_node(v)

        land_use = nx.get_node_attributes(G2,'land_use')
        zonev = nx.get_node_attributes(G,'zone')
        zone = parcel.zone
        for edge in G2.edges():
            if land_use[edge[0]] == land_use[edge[1]]:
                G2[edge[0]][edge[1]]['weight'] = 0.000001
            else:
                G2[edge[0]][edge[1]]['weight'] = 10

        # minimum spanning tree
        #print("graph G2")
        #print(G2.nodes())
        T = nx.minimum_spanning_tree(G2)
        #print("minimum spanning tree T")
        #print(T.nodes())
        #print(len(T.nodes()))
        allSameLUC = True
        costs = {}
        for edge in T.edges(data=True):
            #print("CHECK 0")
            if edge[2]['weight'] > 1:
                allSameLuc = False
                # remove edge and obtain the two connected components of T
                T.remove_edge(*edge[:2])
                components = nx.connected_components(T)
                set1 = next(components)
                set2 = next(components)
                p1 = Parcel(G, frozenset(set1),zone)
                p2 = Parcel(G, frozenset(set2),zone)
                diff_cost = parcel.cost - p1.cost - p2.cost
                #print("diff_cost: " + str(diff_cost))
                #print([p1,p2])
                #print("parcel.area: " + str(parcel.area))
                #print("p1.area: " + str(p1.area) + ", p2.area: " + str(p2.area))
                if p1.area > options['Min_Area'] and p2.area > options['Min_Area']:
                    costs[(p1,p2)] = diff_cost
                # put edge back
                T.add_edge(*edge[:2])

        if allSameLUC:
            for edge in T.edges(data=True):
                # remove edge and obtain the two connected components of T
                T.remove_edge(*edge[:2])
                components = nx.connected_components(T)
                set1 = next(components)
                set2 = next(components)
                #print("components")
                #print(set1)
                #print(len(set1))
                #print(set2)
                #print(len(set2))
                p1 = Parcel(G, frozenset(set1),zone)
                p2 = Parcel(G, frozenset(set2),zone)
                #print("parcel.area: " + str(parcel.area))
                #print("p1.area: " + str(p1.area) + ", p2.area: " + str(p2.area))
                if p1.area > options['Min_Area'] and p2.area > options['Min_Area']:
                    costs[(p1,p2)] = 1 / abs(p1.area - p2.area)
                # put edge back
                T.add_edge(*edge[:2])
                
        # order costs decreasingly by value
        if len(costs) > 0:
            sorted_keys = sorted(costs, key=costs.get, reverse=True)        
            correspondance[sorted_keys[0]] = parcel
            dis_costs[sorted_keys[0]] = costs[sorted_keys[0]]

    sorted_costs = sorted(dis_costs, key=dis_costs.get, reverse=True)
    #print("len(sorted_costs) = " + str(len(sorted_costs)))
    for i in range(0,min(len(sorted_costs),int(options['Min_Parcels'])-len(sol_parcels))):
        pp = sorted_costs[i]
        sol_parcels.remove(correspondance[pp])
        sol_parcels.append(pp[0])
        sol_parcels.append(pp[1])


def agg_feasible(G,p1,p2,options):
    # first check the area
    if p1.area + p2.area > float(options['Max_Area']):
        return False
    adjacent = False
    p1Vlist = list(p1.V)
    cnt = 0
    while not adjacent and cnt < len(p1Vlist):
        node = p1Vlist[cnt]
        neighbors = G.neighbors(node)
        if set(neighbors) & p2.V:
            adjacent = True

        cnt += 1

    return adjacent
        
def aggregate(G,sol_parcels,options):
    # first sort the parcels per zones
    pzones = {}
    cnt = 0
    for p in sol_parcels:
        if p.zone not in pzones:
            pzones[p.zone] = [cnt]
        else:
            pzones[p.zone].append(cnt)
        cnt += 1
    # for each zone consider the less costly feasible aggregation of two parcels
    # feasible: area <= a_max and adjacent parcels
    # store the min cost aggregation for each parcel and select the minimum
    costs_agg = {}
    for k,v in pzones.items():
        min_cost = float("inf")
        pp = ()
        for pair in itertools.combinations(v,2):
            p1 = sol_parcels[pair[0]]
            p2 = sol_parcels[pair[1]]
            if agg_feasible(G,p1,p2,options):
                p_agg = Parcel(G, p1.V|p2.V,k)
                cost = p_agg.cost - p1.cost - p2.cost
                if cost < min_cost:
                    pp = pair
                    min_cost = cost

        if len(pp) > 0:
            costs_agg[pp] = min_cost

    # if any feasible aggregation then choose the less costly
    if costs_agg:
        sorted_costs = sorted(costs_agg.items(), key=operator.itemgetter(1))
        p1 = sol_parcels[sorted_costs[0][0][0]]
        p2 = sol_parcels[sorted_costs[0][0][1]]
        new_p = Parcel(G, p1.V|p2.V,p1.zone)
        sol_parcels.remove(p1)
        sol_parcels.remove(p2)
        sol_parcels.append(new_p)
        
            
def primalHeuristic(G, Gsorig, options, node, area_lucs, polygons, list_nodes):
    parcels = node.set_parcels
    xsol = node.sol
    # make a local copy of the block graphs
    Gs = {}
    for k,v in Gsorig.items():
        Gs[k] = v.copy()
        
    min_area_parcel = float(options['Min_Area'])
    max_area_parcel = float(options['Max_Area']) 
    min_parcels = int(options['Min_Parcels'])
    max_parcels = int(options['Max_Parcels'])

    # include all possible x[p] >= 0.5
    # first order xsol dict
    sorted_xsol = sorted(xsol.items(), key=operator.itemgetter(1), reverse=True)
    sol_parcels = []
    np = 0
    covered_nodes = set()
    for pair in sorted_xsol:
        if pair[1] >= 0.5:
            p = pair[0]
            if not covered_nodes & p.V:     # to avoid overlapping between parcels
                #print("zone: " + p.zone)
                Gaux = Gs[p.zone].copy()
                Gs[p.zone].remove_nodes_from(p.V)
                np += 1
                if not feas_general(Gs,np,options):
                    Gs[p.zone] = Gaux
                    np -= 1
                else:
                    #print(" parcel added from the integer solution, checking feasibility")
                    feas = feas_parcel(G, p, options)
                    if feas:
                        sol_parcels.append(p)
                        covered_nodes |= p.V  # union of both sets
                    

    #print(" heuristic progress: " + str(len(sol_parcels)) + " parcels added, " + str(len(covered_nodes)) + " nodes covered")
    # number of connected components that are left 
    heur_parcels = {}
    heur_parcels1 = {}
    for k,v in Gs.items():
        ccs = nx.connected_components(v)
        for cc in ccs:
            divideParcel(v, Parcel(v, frozenset(cc),k), max_area_parcel, heur_parcels, options)
                
            
    for k,v in heur_parcels.items():
        sol_parcels.append(v)

    nnodesHeur = 0
    for p in sol_parcels:
        nnodesHeur += len(p.V)

    # if #parcels is not the target one, then either aggregate or disaggregate parcels
    while len(sol_parcels) < int(options['Min_Parcels']):
        disaggregate(G,sol_parcels,options)
        #print(" DISAGGREGATION: heuristic progress: " + str(len(sol_parcels)) + " parcels added, " + str(len(covered_nodes)) + " nodes covered")

    ctr = 0
    while len(sol_parcels) > int(options['Min_Parcels']) and ctr != len(sol_parcels):
        ctr = len(sol_parcels)
        aggregate(G,sol_parcels,options)
        #print(" AGGREGATION: heuristic progress: " + str(len(sol_parcels)) + " parcels added, " + str(len(covered_nodes)) + " nodes covered")

    # calculate cost of this solution        
    cost = 0
    sol_lucs = {}
    for p in sol_parcels:
        cost += p.cost
        if p.land_use not in sol_lucs:
            sol_lucs[p.land_use] = p.area
        else:
            sol_lucs[p.land_use] += p.area

    theta = float(options['Theta'])
    for k,v in area_lucs.items():
        if k in sol_lucs:
            cost += theta * abs(area_lucs[k] - sol_lucs[k])
        else:
            cost += theta * area_lucs[k]

    node.ub = cost
    node.solInt = sol_parcels

    nnodesHeur = 0
    for p in sol_parcels:
        nnodesHeur += len(p.V)
        
    # print("from HEURISTIC, solution with " + str(len(sol_parcels)) + " parcels, covering " + str(nnodesHeur) + " nodes from a total of " + str(len(G.nodes())) + "\n    with a cost of " + str(cost) + "\n")

    # print("checking feasibility initial heuristic solution")
    if not feas_solution(G, sol_parcels, options):
        sys.exit(0)

    # generate the geojson output file
    boundaries = []
    lucs = []
    areas = []
    setV = set()
    for p in node.solInt:
        polys = []
        for v in p.V:
            if num_nested_lists(polygons[list_nodes[v]]) == 2:
                polys.append(Polygon(polygons[list_nodes[v]]))
            else:
                polys.append(Polygon(polygons[list_nodes[v]][0]))
                
            setV.add(v)        
        
        boundaries.append(cascaded_union(polys))
        lucs.append(p.land_use)
        areas.append(p.area)

    if len(setV) != len(list_nodes):
        sys.exit("Error while plotting, not all nodes are there, len(setV)= " + str(len(setV)) + " , len(list_nodes): " + str(len(list_nodes)))

    cnt = 0
    feats = []
    for boundary in boundaries:
        feat = geojson.Feature(geometry=boundary)
        feat['properties']['Area'] = areas[cnt]
        feat['properties']['MAJORITY'] = lucs[cnt]
        feat['properties']['id'] = cnt
        feats.append(feat)
        cnt += 1
    
    geom_in_geojson = geojson.FeatureCollection(feats)

    return geom_in_geojson

def  num_nested_lists(l):
    cnt = 0
    if isinstance(l, list):
        cnt += 1 + num_nested_lists(l[0])
    return cnt

def cbg2gj(G, polygons, list_nodes):
    set_parcelsV = {}
    set_parcels = []
    for k, d in G.nodes(data=True):
        if d['zone'] not in set_parcelsV:
            set_parcelsV[d['zone']] = [k]
        else:
            set_parcelsV[d['zone']].append(k)

    for k, d in set_parcelsV.items():
        set_parcels.append(Parcel(G, frozenset(d),k))

    # generate the geojson output file
    boundaries = []
    lucs = []
    areas = []
    setV = set()
    for p in set_parcels:
        polys = []
        for v in p.V:
            if num_nested_lists(polygons[list_nodes[v]]) == 2:
                polys.append(Polygon(polygons[list_nodes[v]]))
            else:
                polys.append(Polygon(polygons[list_nodes[v]][0]))
                
            setV.add(v)        
        
        boundaries.append(cascaded_union(polys))
        lucs.append(p.land_use)
        areas.append(p.area)

    cnt = 0
    feats = []
    for boundary in boundaries:
        feat = geojson.Feature(geometry=boundary)
        feat['properties']['Area'] = areas[cnt]
        feat['properties']['MAJORITY'] = lucs[cnt]
        feat['properties']['id'] = cnt
        feats.append(feat)
        cnt += 1
    
    geom_in_geojson = geojson.FeatureCollection(feats)

    return geom_in_geojson
    
