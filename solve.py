# -*- coding: utf-8 -*-

from gurobipy import *
from pricing import check_columns

def compute_land_use(G,L):
    AL = {}
    for lu in L:
        AL[lu] = 0.0
    for k, d in G.nodes(data=True):
        av = d['area']
        luv = d['land_use']
        if luv not in AL:
            print("Error: Land use code %s not defined in Land Use Set" % luv)
        AL[luv] = AL[luv] + av
    return AL

def print_sol(parcels,m,x,u,v):
    
     print("Selected parcels: ")
     xsol = m.getAttr('x', x)
     for p in parcels:
         if xsol[p] > 0.0001:
             print('%s %.4f' % (p, xsol[p]))

def solve(node,G,L,options):
    x = {}
    u = {}
    o = {}
    y = {}
    
    theta = float(options['Theta'])
    phi = float(options['Phi'])
    min_parcels = int(options['Min_Parcels'])
    max_parcels = int(options['Max_Parcels'])
    min_LUCs = int(options['Min_LUCs'])
    max_LUCs = int(options['Max_LUCs'])
    

    m = Model("AISL")
    # Create variables
    for p in node.parcels:
#        x[p] = m.addVar(vtype=GRB.BINARY, name='x_%d' % p.ix)
         x[p] = m.addVar(lb=0, ub=1, vtype=GRB.CONTINUOUS, name='x_%d' % p.ix)
    for i in L:
        u[i] = m.addVar(lb=0,name='u_%s' % i)
        o[i] = m.addVar(lb=0,name='o_%s' % i)
#        y[i] = m.addVar(lb=0, ub=1, vtype=GRB.CONTINUOUS, name='y_%s' % i)
    # Need to update to add the variables
    m.update()
    # Create Objective
    obj = LinExpr()
    for p in node.parcels:
        obj.addTerms(p.cost, x[p])
    for i in L:
        obj.addTerms(theta, u[i])
        obj.addTerms(theta, o[i])

#    for p in parcels:
#        obj.addTerms(phi, x[p])

    m.setObjective(obj, GRB.MINIMIZE)
    
    #Add Constraints
    # Constraint 1: Coverage: sum{p} a_{pv} x_p = 1 forall v
    for v in G.nodes():
        le = LinExpr()
        for p in node.parcels:
            if v in p.V:
                le.addTerms(1.0, x[p])
        m.addConstr(le, GRB.GREATER_EQUAL, 1.0, 'cover_%d' % v)
    # Constraint 2: sum{p} beta_{pi} x_p + u_i - l_i forall i in L 
    # First compute toal land use
    AL = compute_land_use(G,L)
    for i in L:
        le = LinExpr()
        for p in node.parcels:
            if i == p.land_use:
                le.addTerms(p.area, x[p])
        le.addTerms(1.0, u[i])
        le.addTerms(-1.0,o[i])
        m.addConstr(le, GRB.EQUAL, AL[i], 'land_use_%s' % i)
    # Constraint 3: Cardinality
    le = LinExpr()
    for p in node.parcels:
        le.addTerms(1.0,x[p])    
    m.addRange(le, min_parcels, max_parcels, "Cardinality")

    # # Constraint 4: limit #LUCs
    # le_y = LinExpr()
    # for l in L:
    #     le_y.addTerms(1.0, y[l])
        
    # m.addRange(le_y, min_LUCs, max_LUCs, "LUCsRange")

    # # Constraint 5: relate x_p and y_l
    # for l in L:
    #     le_xy = LinExpr()
    #     cnt = 0.0
    #     for p in parcels:
    #         if p.land_use == l:
    #             cnt = cnt + 1
    #             le_xy.addTerms(1.0, x[p])

    #     le_xy.addTerms(-cnt, y[l])
    #     m.addConstr(le_xy, GRB.LESS_EQUAL, 0.0, 'compatibility_LUC_%s' %l )


    # impose fix1 variables
    for p in node.fix1:
        m.addConstr(x[p] == 1.0, 'fix1_%d' %p.ix)

    # impose fix0 variables
    for p in node.fix0:
        m.addConstr(x[p] == 0.0, 'fix0_%d' %p.ix)

        
    m.setParam('OutputFlag', False)    
    m.optimize()
    status = m.status
    if status ==  GRB.Status.INF_OR_UNBD or status == GRB.Status.INFEASIBLE \
       or status == GRB.Status.UNBOUNDED:
        print('The model is infeasible or unbounded')
        return(1)
#    m.write("parcel.lp")
#    print_sol(parcels,m,x,u,v)
       

    # get #parcels fractionality
    xsol = m.getAttr('x', x)
    n_parcels = 0
    fract = 0
    fract_var = 0
    int_var = set()
    for p in node.parcels:
        node.sol[p] = xsol[p]
        if xsol[p] > 0.0001:
            n_parcels += xsol[p]
            if xsol[p] < 0.99999:
                fract_var += 1
                fract += xsol[p]

        if xsol[p] > 0.9999:
            int_var.add(p)
            
 #   print('%s %.4f' % (p, xsol[p]))
    print '#parcels : %g'%n_parcels
    print '#fract_var : %g'%fract_var 
    print '#fract : %g'%fract
   
    if fract == 0.0:
        print("integer optimal solution!")
        for p in int_var:
            print(p)
    
    # n_LUCs = 0
    # ysol = m.getAttr('x', y)
    # for l in L:
    #     if ysol[l] > 0.0001:
    #         n_LUCs = n_LUCs + ysol[l]
        
    # print '#LUCs : %g' % n_LUCs


    # get duals
    node.duals= {}
    for c in m.getConstrs():
        node.duals[c.constrName] = c.pi
#        print 'the dual value of %s : %g'%(c.constrName,c.pi)


    

    # # check reduced costs
    # xRC = m.getAttr('RC', x)
    # xVBasis = m.getAttr('VBasis', x)
    # for p in node.parcels:
    #     if xsol[p] > -0.0001:
    #         print 'vBasis %d and reduced cost parcel %d is %.4f' %(xVBasis[p],p.ix, xRC[p])
    # #check_columns(duals, node.parcels, xsol,L,)
    node.lb = m.getAttr(GRB.Attr.ObjVal)
    return 0
        
            
        
    
