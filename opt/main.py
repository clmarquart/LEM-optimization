# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 11:05:19 2018

@author: linderot
"""

import os
import sys
import re
import networkx as nx
import geojson
import StringIO

from parcel import Parcel
from feasibility import feas_general, feas_solution, feas_parcel
from tree import Node
from heuristic import primalHeuristic, cbg2gj

def build_graph(in_str, G, LUCs, select_cbg, options, list_nodes, polygons, LUC_list):
    max_vertex=1000000
    
    cnt_parcels = {}
    neighbors = {}
    
            
    # select read census blocks

    gj_parcels = geojson.loads(in_str)
    feat_parcels = gj_parcels['features']
    dict_nodes = {}
    cnt = 0
    for feature in feat_parcels:
        if feature['properties']["GISJOINcbg"] not in select_cbg:
            select_cbg.add(feature['properties']["GISJOINcbg"])
            cnt_parcels[feature['properties']["GISJOINcbg"]] = 1
        else:
            cnt_parcels[feature['properties']["GISJOINcbg"]] += 1
            
        node_id = feature['properties']["src_GISJOI"]
        # add polygon
        poly = feature['geometry']
        polygons[node_id] = poly["coordinates"][0]

        # construct the graph
        ind = cnt
        if node_id in dict_nodes:
            ind = dict_nodes[node_id]
        else:
            list_nodes.append(node_id)
            dict_nodes[node_id] = cnt
            cnt += 1

        G.add_node(ind, area=float(feature['properties']["AreaAC"]),
                   land_use=feature['properties']["MAJORITY"],
                   latitude=float(feature['properties']["Lat"]),
                   longitude=float(feature['properties']["Long"]),
                   zone=feature['properties']["GISJOINcbg"])


        LUC_list.append(feature['properties']["MAJORITY"])
        LUCs.add(feature['properties']["MAJORITY"])
        # save neighbors
        neighbors[node_id] = feature['properties']["nbr_GISJOI"].split(',')


    # add edges now
    for k, v in neighbors.items():
        for n in v:
            if n in dict_nodes:
                G.add_edge(dict_nodes[k], dict_nodes[n])
            
                
    # # print out how many parcels per cbg there are
    # cnt2 = 0
    # print(" parcels per cbg")
    # for k, v in cnt_parcels.items():
    #     cnt2 += v
    #     print(k + ": " + str(v) + " parcels")

    # print("in total, there are " + str(cnt) + " parcels, another count is " + str(cnt2))
    # print(" and there are " + str(len(cnt_parcels.keys())) + " cbg")

def make_zone_graphs(G, Gs, LUCs, zones):
    nodes = {}
    for k, d in G.nodes(data=True):
        if d['zone'] not in zones:
            Gs[d['zone']] = nx.Graph()
            zones.append(d['zone'])
            nodes[d['zone']] = []
            LUCs[d['zone']] = set()

        nodes[d['zone']].append(k)
        LUCs[d['zone']].add(d['land_use'])

    for k,v in nodes.items():
        Gs[k] = G.subgraph(v) 
                
def getInitialMap(in_geojson):

    # set options
    options = {"Min_Area":0.0, "Max_Area":0.0, "Min_Parcels":200, "Max_Parcels":200, "Theta":10.0}
    # initialize data structures
    G = nx.Graph()
    L = set()
    select_cbg = set()
    zones = []
    zone_graphs = {}
    zone_LUCs = {}
    list_nodes = []
    polygons = {}
    polygons_cbg = {}
    LUC_list = []
    
    build_graph(in_geojson, G, L, select_cbg, options, list_nodes, polygons, LUC_list)

    make_zone_graphs(G, zone_graphs, zone_LUCs, zones)

    area_lucs = {}
    for v,d in G.nodes(data=True):
        av = d['area']
        luv = d['land_use']
        if luv not in area_lucs:
            area_lucs[luv] = av
        else:
            area_lucs[luv] += av


    # automatically fix min_area and max_area parcel
    total_area = 0
    bigger_parcel_area = 0
    area_cbg = {}
    area = nx.get_node_attributes(G,'area')
    zonev = nx.get_node_attributes(G,'zone')
    for k, v in area.items():
        if zonev[k] not in area_cbg:
            area_cbg[zonev[k]] = v
        else:
            area_cbg[zonev[k]] += v                         
                     
        total_area += v
        if v > bigger_parcel_area:
            bigger_parcel_area = v

    #print("AREA CBGs")
    #for k, v in area_cbg.items():
    #    print(str(v) + ", " + str(k))
        
    min_area_cbg = area_cbg[min(area_cbg, key=area_cbg.get)]
    ave_area_parcel = total_area / 200.
    options['Min_Area'] = min(ave_area_parcel / 4, 0.99*min_area_cbg)
    options['Max_Area'] = max(1.1*bigger_parcel_area, 2*ave_area_parcel)

    node0 = Node(-1,0,[],[], [], set())
    if len(area_cbg) < options['Max_Parcels']:
        out_gj = primalHeuristic(G, zone_graphs, options, node0, area_lucs, polygons, list_nodes)
    else:
        out_gj = cbg2gj(G, polygons, list_nodes)

    output = StringIO.StringIO()
    geojson.dump(out_gj, output)
    return output.getvalue()
                        
# def main():
#     # input geojson file to string
#     in_geojson = ""
#     with open(sys.argv[1], 'r') as f_in:
#         in_geojson = f_in.read()

#     # do the work
#     out_geojson = getInitialMap(in_geojson)

#     # write output to file
#     with open("output.geojson", 'w') as f_out:
#         f_out.write(out_geojson)
        
# if __name__ == "__main__":
#     main()


