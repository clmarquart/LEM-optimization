#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import cgi
import json
from pyArango.connection import *

import os
import sys
import re
import networkx as nx
import geojson
import StringIO

from parcel import Parcel
from feasibility import feas_general, feas_solution, feas_parcel
from tree import Node
from heuristic import primalHeuristic, cbg2gj

PORT_NUMBER = 9000

conn = Connection(arangoURL="http://ec2-34-212-180-115.us-west-2.compute.amazonaws.com:8529", username="root", password="i-00c8a82a55ef77c30")
db = conn["lem-parcels"]
cbgQuery = "FOR p IN parcels FILTER p.layer == \"BlockGroup\" RETURN p"
cbgResults = db.AQLQuery(cbgQuery, rawResults=True, batchSize=500)
print len(cbgResults)

def docToJson(doc):
    print doc
    return geojson.loads(doc)

def build_graph(in_str, G, LUCs, select_cbg, options, list_nodes, polygons, LUC_list):
    max_vertex=1000000
    
    cnt_parcels = {}
    neighbors = {}
    
            
    # select read census blocks

    # gj_parcels = geojson.loads(in_str)
    feat_parcels = in_str #gj_parcels;# ['features']
    dict_nodes = {}
    cnt = 0
    for feature in feat_parcels:
        feature = geojson.loads(feature)
        if feature['properties']["GISJOI_cbg"] not in select_cbg:
            select_cbg.add(feature['properties']["GISJOI_cbg"])
            cnt_parcels[feature['properties']["GISJOI_cbg"]] = 1
        else:
            cnt_parcels[feature['properties']["GISJOI_cbg"]] += 1
            
        node_id = feature['properties']["src_GISJOI"]
        # add polygon
        poly = feature['geometry']
        polygons[node_id] = poly["coordinates"][0]

        # construct the graph
        ind = cnt
        if node_id in dict_nodes:
            ind = dict_nodes[node_id]
        else:
            list_nodes.append(node_id)
            dict_nodes[node_id] = cnt
            cnt += 1

        G.add_node(ind, area=float(feature['properties']["AreaAC"]),
                   land_use=feature['properties']["MAJORITY"],
                   latitude=float(feature['properties']["Lat"]),
                   longitude=float(feature['properties']["Long"]),
                   zone=feature['properties']["GISJOI_cbg"])

        print(float(feature['properties']["MAJORITY"]))
        LUC_list.append(feature['properties']["MAJORITY"])
        LUCs.add(feature['properties']["MAJORITY"])
        # save neighbors
        neighbors[node_id] = feature['properties']["nbr_GISJOI"].split(',')


    # add edges now
    for k, v in neighbors.items():
        for n in v:
            if n in dict_nodes:
                G.add_edge(dict_nodes[k], dict_nodes[n])
            
                
    # # print out how many parcels per cbg there are
    # cnt2 = 0
    # print(" parcels per cbg")
    # for k, v in cnt_parcels.items():
    #     cnt2 += v
    #     print(k + ": " + str(v) + " parcels")

    # print("in total, there are " + str(cnt) + " parcels, another count is " + str(cnt2))
    # print(" and there are " + str(len(cnt_parcels.keys())) + " cbg")

def make_zone_graphs(G, Gs, LUCs, zones):
    nodes = {}
    for k, d in G.nodes(data=True):
        if d['zone'] not in zones:
            Gs[d['zone']] = nx.Graph()
            zones.append(d['zone'])
            nodes[d['zone']] = []
            LUCs[d['zone']] = set()

        nodes[d['zone']].append(k)
        LUCs[d['zone']].add(d['land_use'])

    for k,v in nodes.items():
        Gs[k] = G.subgraph(v)              

def getInitialMap(in_geojson):

    # set options
    options = {"Min_Area":0.0, "Max_Area":0.0, "Min_Parcels":200, "Max_Parcels":200, "Theta":10.0}
    # initialize data structures
    G = nx.Graph()
    L = set()
    select_cbg = set()
    zones = []
    zone_graphs = {}
    zone_LUCs = {}
    list_nodes = []
    polygons = {}
    polygons_cbg = {}
    LUC_list = []
    
    build_graph(in_geojson, G, L, select_cbg, options, list_nodes, polygons, LUC_list)
    
    make_zone_graphs(G, zone_graphs, zone_LUCs, zones)

    area_lucs = {}
    for v,d in G.nodes(data=True):
        av = d['area']
        luv = d['land_use']
        if luv not in area_lucs:
            area_lucs[luv] = av
        else:
            area_lucs[luv] += av

    # automatically fix min_area and max_area parcel
    total_area = 0
    bigger_parcel_area = 0
    area_cbg = {}
    area = nx.get_node_attributes(G,'area')
    zonev = nx.get_node_attributes(G,'zone')
    for k, v in area.items():
        if zonev[k] not in area_cbg:
            area_cbg[zonev[k]] = v
        else:
            area_cbg[zonev[k]] += v                         
                     
        total_area += v
        if v > bigger_parcel_area:
            bigger_parcel_area = v
        
    min_area_cbg = area_cbg[min(area_cbg, key=area_cbg.get)]
    ave_area_parcel = total_area / 200.
    options['Min_Area'] = min(ave_area_parcel / 4, 0.99*min_area_cbg)
    options['Max_Area'] = max(1.1*bigger_parcel_area, 2*ave_area_parcel)

    print "Heuristics"
    node0 = Node(-1,0,[],[], [], set())
    if len(area_cbg) < options['Max_Parcels']:
        print "Primal"
        out_gj = primalHeuristic(G, zone_graphs, options, node0, area_lucs, polygons, list_nodes)
    else:
        print "CBG"
        out_gj = cbg2gj(G, polygons, list_nodes)

    print "Done"
    output = StringIO.StringIO()
    geojson.dump(out_gj, output)
    return output.getvalue()
   

class myHandler(BaseHTTPRequestHandler):
    #Handler for the POST requests
    def do_POST(self):
        if self.path=="/send":
            form = cgi.FieldStorage(
                fp=self.rfile, 
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE': "text/json",
            })
            parcels = json.loads(form["parcels"].value)
            lat = form["lat"].value
            lng = form["lng"].value
            resp = {'name': form["name"].value, 'parcels': parcels}

            aql = "FOR parcel IN NEAR(parcels, " + lat + ", " + lng + ", 220, 'distance') FILTER parcel.layer == 'Block' RETURN parcel"

            # aql = "FOR p IN parcels FILTER p._key IN ['" + "','".join(parcels) + "'] RETURN p"
            queryResult = db.AQLQuery(aql, rawResults=True, batchSize=2)

            document = queryResult[0]
            # gj_parcels = geojson.dumps(document.__str__)
            gj_parcels = map(geojson.dumps, queryResult)
            out_geojson = getInitialMap(gj_parcels)

            self.send_response(200)
            self.send_header("content-type", "text/json")
            self.end_headers()

            # print gj_parcels #.__repr__
            self.wfile.write(gj_parcels)
            # self.wfile.write(json.dumps(document))
            return    

try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
    