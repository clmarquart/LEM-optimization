# -*- coding: utf-8 -*-
"""
Created on Tue Apr  17 08:29:19 2018

@author: jnares
"""
import networkx as nx
from sys import maxint
from math import ceil
#from parcelize import Parcel
import random
import sys


def feas_general(Gs, np, options):
    # np: number of parcels already defined
    cc = 0 # total connected components
    min_cc_parcels = 0
    max_area_parcel = float(options['Max_Area'])
    min_area_parcel = float(options['Min_Area'])
    min_area = maxint
    max_parcels = int(options['Max_Parcels'])
    for k,v in Gs.items():
        cc += nx.number_connected_components(v)
        area= sum(nx.get_node_attributes(v,'area'))
        min_cc_parcels += ceil(area/max_area_parcel)
        if area < min_area:
            min_area = area
            
    if cc > max_parcels - np or min_area < min_area_parcel or min_cc_parcels > max_parcels - np:
        return False
    else:
        return True

    #     print("cc: " + str(cc))
    #     print("max_parcels: " + str(max_parcels))
    #     print("np: " + str(np))
    #     print("min_cc_parcels: " + str(min_cc_parcels))
    #     print("cc > max_parcels : " + str(cc > max_parcels))
    #     print("min_area < min_area_parcel : " + str(min_area < min_area_parcel))
    #     print("min_cc_parcels > max_parcels - np : " + str(min_cc_parcels > max_parcels - np))      

    
def feas_parcel(G, parcel, options):
    if len(parcel.V) == 1:
        return True
    
    max_area_parcel = float(options['Max_Area'])
    min_area_parcel = float(options['Min_Area'])
    nodes = G.nodes()
    if not parcel.V.issubset(nodes):
        #print(" error, parcel nodes are not contained in zone graph")
        return False
    if parcel.area < min_area_parcel or parcel.area > max_area_parcel:
        #print(" error, parcel area not valid")
        #print("parcel.area=" + str(parcel.area))
        #print("min_area_parcel: " + str(min_area_parcel) + ", max_area_parcel: " + str(max_area_parcel))
        return False
    H=G.copy()
    for n in nodes:
        if n not in parcel.V:
            H.remove_node(n)

    if nx.number_connected_components(H) > 1:
        #print(" error, parcel is not connected")
        #print(str(nx.number_connected_components(H)) + " components. Nodes H: ")
        #print(H.nodes())
        
        return False
    
    return True

def feas_solution(G, parcels, options):
    covering = [0] * len(G)
    for parcel in parcels:
        if not feas_parcel(G, parcel, options):
            sys.exit("Error: one parcel is not feasible, heuristic solution is not valid!")
        else:
            for v in parcel.V:
                covering[v-1] += 1

    if max(covering) > 1 or min(covering) < 1:
        sys.exit("Error: not all nodes are covered, heuristic solution is not valid!")

    return True


def connected_parcel(G, parcel, options):
    nodes = G.nodes()
    H=G.copy()
    for n in nodes:
        if n not in parcel.V:
            H.remove_node(n)

    if nx.number_connected_components(H) > 1:
        print(" error, parcel is not connected")
        print(str(nx.number_connected_components(H)) + " components. Nodes H: ")
        print(H.nodes())
        
        return False
    
    return True
